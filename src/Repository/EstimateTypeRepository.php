<?php

namespace App\Repository;

use App\Entity\EstimateType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EstimateType|null find($id, $lockMode = null, $lockVersion = null)
 * @method EstimateType|null findOneBy(array $criteria, array $orderBy = null)
 * @method EstimateType[]    findAll()
 * @method EstimateType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EstimateTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EstimateType::class);
    }

    // /**
    //  * @return EstimateType[] Returns an array of EstimateType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EstimateType
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
