<?php

namespace App\Controller;

use App\Entity\Type;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Content;
use App\Form\ContentType;
use App\Service\UploadFileService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class DashboardController extends AbstractController
{
    private $encoders;
    private $normalizers;
    private $serializer;
    public function __construct()
    {
        $this->encoders = [new JsonEncoder()];
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object;
            },
        ];
        $this->normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];
        $this->serializer = new Serializer($this->normalizers, $this->encoders);
    }
    /**
     * @Route("/", name="dashboard", options={"expose"=true})
     */
    public function index()
    {
        return $this->render('dashboard/index.html.twig');
    }

    /**
     * @Route("/find/all/type", name="type.findAll", options={"expose"=true})
     */
    public function findAllType()
    {
        $em = $this->getDoctrine()->getManager();
        $allTypes = $em->getRepository(Type::class)->findAll();
        $allTypesJson = $this->serializer->serialize($allTypes, 'json', ['groups' => ['normal']]);
        return JsonResponse::fromJsonString($allTypesJson);
    }

    /**
     * @Route("/find/all/by/type/{type}", name="type.findAllByType", options={"expose"=true})
     */
    public function getContentByType(Type $type)
    {
        $em = $this->getDoctrine()->getManager();
        $allContentsByType = $em->getRepository(Type::class)->find($type);
        $allContentJson = $this->serializer->serialize($allContentsByType, 'json', ['groups' => ['normal']]);
        return JsonResponse::fromJsonString($allContentJson);
    }

    /**
     * @Route("/delete/content/{content}", name="content.remove", options={"expose"=true})
     */
    public function deleteContentById(Content $content)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($content);
        $em->flush();
        return $this->json(['status' => 'ok']);
    }

    /**
     * @Route("/edit/content/{content}", name="content.edit", options={"expose"=true})
     */
    public function editContent(Content $content)
    {
        $form = $this->createForm(ContentType::class, $content);
        return $this->render('dashboard/edit.html.twig', [
            'form' => $form->createView(),
            'content' => $content
        ]);
    }

    /**
     * @Route("/add/content/{id}", name="content.add", options={"expose"=true})
     */
    public function addContent($id)
    {
        $content = new Content();
        $form = $this->createForm(ContentType::class, $content);
        return $this->render('dashboard/add.html.twig', [
            'form' => $form->createView(),
            'idContent' => $id
        ]);
    }

    /**
     * @Route("/valide/edit/content/{content}", name="content.valid", options={"expose"=true})
     */
    public function valideContent(Request $request, Content $content, UploadFileService $uploadFileService)
    {
        $form = $this->createForm(ContentType::class, $content);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $fileData = $form->get('img')->getData();
            $path = $this->getParameter('file_upload_dir');
            $newFileName = $uploadFileService->doUpload($fileData, $path);
            if ($newFileName) {
                $content->setImg($newFileName);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($content);
            $em->flush();
        }
        $allContentJson = $this->serializer->serialize($content, 'json', ['groups' => ['normal']]);
        return JsonResponse::fromJsonString($allContentJson);
    }

    /**
     * @Route("/valide/new/content/{content}", name="content.validNew", options={"expose"=true})
     */
    public function valideAddContent(Request $request,Content $content, UploadFileService $uploadFileService)
    {
        $lastContent = $content;
        $content = new Content();
        $form = $this->createForm(ContentType::class, $content);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $fileData = $form->get('img')->getData();
            $path = $this->getParameter('file_upload_dir');
            $newFileName = $uploadFileService->doUpload($fileData, $path);
            if ($newFileName) {
                $content->setImg($newFileName);
            }
            $content->setContent($lastContent);
            $em = $this->getDoctrine()->getManager();
            $em->persist($content);
            $em->flush();
        }
        $allContentJson = $this->serializer->serialize($content, 'json', ['groups' => ['normal']]);
        return JsonResponse::fromJsonString($allContentJson);
    }

    /**
     * @Route("/api/get/content/", name="content.get", options={"expose"=true})
     */
    public function getContentApi()
    {
        $em = $this->getDoctrine()->getManager();
        $types = $em->getRepository(Type::class)->findAll();
        $types = $this->serializer->normalize($types);
        $data = [];
        foreach ($types as $keyType => $type) {
            $data[$type['name']] = $this->removeUnUsedIndex('type', $type['contents']);
            $data[$type['name']]  = $this->getChild($data[$type['name']]);
        }
        $dataJson = $this->serializer->serialize($data, 'json', ['groups' => ['normal']]);
        return JsonResponse::fromJsonString($dataJson);
    }

    private function removeUnUsedIndex($index, $array)
    {
        foreach ($array as $key => $value) {
            unset($array[$key][$index]);
            unset($array[$key]['content']);
        }
        return $array;
    }

    private function removeUnUsedIndexChild($index, $array)
    {
        foreach ($array as $key => $value) {
            unset($array[$key][$index]);
            unset($array[$key]['content']);
            unset($array[$key]['childs']);
        }
        return $array;
    }

    private function getChild($data)
    {
        foreach ($data as $key => $value) {
            if (count($value['childs'])) {
                $arrayData = $this->serializer->normalize($value['childs']);
                $data[$key]['childs'] = $this->removeUnUsedIndexChild('type', $arrayData);
            }
        }
        return $data;
    }
}
