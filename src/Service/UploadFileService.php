<?php

namespace App\Service;

use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class UploadFileService
{
    private $sluggerInterface;

    public function __construct(SluggerInterface $sluggerInterface)
    {
        $this->sluggerInterface = $sluggerInterface;
    }

    public function doUpload($fileData,$path) {
        if($fileData) {
            $originalFilename = pathinfo($fileData->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $this->sluggerInterface->slug($originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $fileData->guessExtension();
            try {
                $fileData->move(
                    $path,
                    $newFilename
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }
            return $newFilename;
        }else {
            return false;
        }
    }
}
