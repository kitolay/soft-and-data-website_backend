-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 27, 2020 at 09:02 AM
-- Server version: 8.0.20-0ubuntu0.20.04.1
-- PHP Version: 7.3.20-1+ubuntu20.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tech`
--

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int NOT NULL,
  `title1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `full_description` longtext COLLATE utf8mb4_unicode_ci,
  `type_id` int DEFAULT NULL,
  `content_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `title1`, `title2`, `img`, `description`, `full_description`, `type_id`, `content_id`) VALUES
(18, 'NOS EXPERIENCES', NULL, NULL, NULL, NULL, 9, NULL),
(19, 'Angular', NULL, NULL, '60', NULL, NULL, 18),
(20, 'Symfony', NULL, NULL, '90', NULL, NULL, 18),
(24, 'Qui sommes-nous?', NULL, NULL, 'Lorem ipsum dolor hamet', NULL, 3, NULL),
(26, 'Notre compétence', NULL, NULL, 'Lorem ipsum dolor hamet', NULL, 4, NULL),
(27, 'Web et Ecommerce', 'Pour vous présentés', '1-5f1c4e219b25c.png', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit aperiam expedita sequi atque inventore temporibus, eveniet eligendi recusandae quos in at, nam consequatur, itaque culpa neque pariatur quas perspiciatis sapiente?', NULL, NULL, 26),
(28, 'Mobile', 'Lorem Lorem ipsum', '1-5f1c4e7c9d789.png', 'Lorem ipsum ipsum ipsum', NULL, NULL, 26),
(29, 'Ampoule', 'Lorem dolor dolor', '1-5f1c4eac2ec69.png', 'Lorem dolor ipsum hamet dolor', NULL, NULL, 26),
(30, 'LOREM IPSUM1', NULL, NULL, 'Lorem leomrnlknczn lsjkblas ipsum', NULL, NULL, 24),
(31, 'LOREM IPSUM 2', NULL, NULL, 'Lorem ipsum lorem ipsum bbe', NULL, NULL, 24),
(32, 'LOREL IPSUMNJ KB  S', NULL, NULL, 'Lorem biddn huzdnzvkdj sljdbus  kjsbdkjd slkjd', NULL, NULL, 24),
(33, 'Création graphique', NULL, NULL, NULL, NULL, 5, NULL),
(34, 'AFFICHES / PLAQUETTES / BROCHURES / CATALOGUES', NULL, '1-5f1c55ee722f4.png', 'L’affiche est l’outil de communication le plus important et facilite la communication avec le grand public. Il est idéal pour présenter un événement, un nouveau produit ou un nouveau concept. Création d’une brochure pour un produit et une entreprise. Que ce soit le format d’affiche que vous souhaitez présenter avec des outils de publicité et de communication pour mettre en valeur votre activité ou vos produits, notre graphiste est assuré de la personnalisation de votre campagne. Le panneau “4 sur 3”, roll-up, une simple feuille A4 où un multiformat, vinyle, étiquettes, goodies publicitaires, packaging, habillage stands, panneaux, enseignes, Covering véhicule, ….', NULL, NULL, 33),
(35, 'LOGOTYPESS', NULL, '1-5f1c561ea6a8f.png', 'Le logotype est un élément important et la base de toute identité visuelle. C’est un élément graphique composé d’une forme, d’un objet, d’une couleur qui permet d’exprimer ses valeurs d’entreprise où ses produits et contribue en effet à l’image de marque. Le logotype est un symbole identifiant une structure, une institution, une entreprise ou une marque. Befiana Web Plus s’engage à créer avec vous un logo unique à l’image de votre entreprise et à décliner selon les règles de la communication pour une parfaite adaptation aux différents supports.', NULL, NULL, 33),
(36, 'WEB DESIGN', NULL, '1-5f1c5647129f5.png', 'rfdskneovsenfvqîdojf^odfiqodfnv odsbvoiubdcv sxcvjbsqiegbvSV', NULL, NULL, 33),
(37, 'CARTE DE VISITE / FLYERS / DÉPLIANTS', NULL, '1-5f1c56744703d.png', 'ijbxpcdjvbqpod qosjbqo sdvqkjsboq skbj nbpisZNDovnokkkkkkkkkkkkkkkdvdsklcv swodbv KJSDBOVÔJBC KJBOu bosdbvsojnsxksdbbbbbbbbbbbbbslkcbjovç s', NULL, NULL, 33),
(38, 'Création site Internet et Application', NULL, '1-5f1c59a4a6608.png', 'lfbsojbsùojnùqosnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnùqkdnvqùksncqsklnv', NULL, 7, NULL),
(39, 'CMS Open Source', NULL, '1-5f1c59d3c0c1f.png', 'Développement et intégration avec les CMS Open Source : WordPress, Joomla, Prestashop, … Compatibilité, performance et rentabilité. Développez et personnalisez un thème puissant et sécurisé sur WordPress avec le meilleur framework Genesis. Et comme Gantry Framework pour Joomla. Profitez d’un soutien illimité sur votre projet et mettez à jour sur votre propre site en un clic.', NULL, NULL, 38),
(40, 'Des développements sur mesure', NULL, '1-5f1c59f5ac6c1.png', 'dgngsfh,tghhhhhhhhhhhhhhhhhhdh,dxh,dh,', NULL, NULL, 38),
(41, 'CMS Open Source', NULL, '1-5f1c5a1f545e1.png', 'dsdhvÔID CHVBQISCHÔAVIUZBODVI', NULL, NULL, 38),
(42, 'CREATION CREATION', NULL, '1-5f1c5a5e831c2.png', 'csqkjGS mqsjidi pwugôqHDOŜIDH  UG PISUGDV Z', NULL, NULL, 38),
(43, 'LES TECHNOLOGIES UTILISEES', NULL, NULL, NULL, NULL, 8, NULL),
(44, NULL, NULL, '1-5f1c5c7114c0f.png', NULL, NULL, NULL, 43),
(45, NULL, NULL, '1-5f1c5cb2431de.png', NULL, NULL, NULL, 43),
(46, NULL, NULL, 'logo-5f1c5cd39d58f.png', NULL, NULL, NULL, 43),
(47, NULL, NULL, 'logo-5f1c5cde91844.png', NULL, NULL, NULL, 43),
(48, NULL, NULL, '1-5f1c5cec7eca2.png', NULL, NULL, NULL, 43),
(49, NULL, NULL, 'logo-5f1c5cfab1ac4.png', NULL, NULL, NULL, 43),
(50, 'Gestion des slides', NULL, NULL, NULL, NULL, 10, NULL),
(51, NULL, NULL, '1-5f1c781388dd7.jpeg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci esse vitae exercitationem fugit, numquam minus!', NULL, NULL, 50),
(52, 'titre 1', NULL, '2-5f1c792152940.jpeg', 'test', NULL, NULL, 50);

-- --------------------------------------------------------

--
-- Table structure for table `estimate_type`
--

CREATE TABLE `estimate_type` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `name`, `title`) VALUES
(3, 'who_are_we', 'Qui sommes-nous'),
(4, 'our_comp', 'Notre competence'),
(5, 'graphic', 'Graphique'),
(7, 'web_create', 'Création site Web'),
(8, 'techno_use', 'Les technologies Utilisées'),
(9, 'experience', 'Nos experiences'),
(10, 'slide', 'Slide');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_FEC530A9C54C8C93` (`type_id`),
  ADD KEY `IDX_FEC530A984A0A3ED` (`content_id`);

--
-- Indexes for table `estimate_type`
--
ALTER TABLE `estimate_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `estimate_type`
--
ALTER TABLE `estimate_type`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `content`
--
ALTER TABLE `content`
  ADD CONSTRAINT `FK_FEC530A984A0A3ED` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`),
  ADD CONSTRAINT `FK_FEC530A9C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
