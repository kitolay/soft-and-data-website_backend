import angular from 'angular';
import ngAnimate from 'angular-animate';
import ngMaterial from 'angular-material';
import Routing from '../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min';
const routes = require('../../js/fos_js_routes.json');
Routing.setRoutingData(routes);

console.log(Routing.generate('dashboard'));
var app = angular.module('app', [ngAnimate, ngMaterial]);

app.controller('tableCtrl', function ($scope, $http, $mdDialog) {

    $scope.initTable = function () {
        $http.get(Routing.generate('type.findAll')).then(
            function (allTypes) {
                $scope.allTypes = allTypes.data;
                $scope.contents = allTypes.data[0].contents;
                $scope.selected_type = allTypes.data[0].id;
            }
        )
    }

    $scope.changeType = function () {
        getContentByType($scope.selected_type).then(function (allTypes) {
            $scope.contents = allTypes.data.contents;
            console.log($scope.contents);
        })
    }

    $scope.showConfirm = function (ev, index) {
        var confirm = $mdDialog.confirm()
            .title('Voulez vous supprimer cette elements')
            .textContent('Cette action est ireverssible')
            .ariaLabel('Lucky day')
            .targetEvent(ev)
            .ok('Supprimer')
            .cancel('Annuler');

        $mdDialog.show(confirm).then(function () {
            $http.get(Routing.generate('content.remove', { content: $scope.contents[0].childs[index].id })).then(function () {
                $scope.contents[0].childs.splice(index, 1);
            })
        }, function () {

        });
    }

    $scope.showEditDialog = function (ev, index, type) {
        if (type == 'parent') {
            var content = $scope.contents[0].id
        } else {
            var content = $scope.contents[0].childs[index].id
        }
        $mdDialog.show({
            controller: DialogController,
            templateUrl: Routing.generate('content.edit', { content: content }),
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true
        }).then(function (answer) {
            alert('close 1');
        }, function () {
            $scope.changeType();
        });
    }

    $scope.showAddDialog = function (ev, index) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: Routing.generate('content.add', { id: $scope.contents[0].id}),
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true
        }).then(function (answer) {
            alert('close 1');
        }, function () {
            $scope.changeType();
        });
    }

    $scope.clickSave = function () {
        alert('ki');
    }

    function getContentByType(selected_type) {
        return $http.get(Routing.generate('type.findAllByType', { type: selected_type }));
    }

    function DialogController($scope, $mdDialog) {
        $scope.hide = function () {
            $mdDialog.hide();
        };
        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        $scope.answer = function (answer) {
            $mdDialog.hide(answer);
        };
    }
})
